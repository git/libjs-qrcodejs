include /usr/share/dpkg/pkg-info.mk

PACKAGE=libjs-qrcodejs
BUILDDIR ?= $(PACKAGE)-$(DEB_VERSION)
ORIG_SRC_TAR=$(PACKAGE)_$(DEB_VERSION_UPSTREAM).orig.tar.gz

DEB=$(PACKAGE)_$(DEB_VERSION)_all.deb
DSC=$(PACKAGE)_$(DEB_VERSION).dsc

GITVERSION:=$(shell git rev-parse HEAD)

all:
	true

$(BUILDDIR):
	rm -rf $(BUILDDIR).tmp
	cp -a src $(BUILDDIR).tmp
	cp -a debian $(BUILDDIR).tmp/
	echo "git clone git://git.proxmox.com/git/libjs-qrcodejs.git\\ngit checkout $(GITVERSION)" >  $(BUILDDIR).tmp/debian/SOURCE
	mv $(BUILDDIR).tmp/ $(BUILDDIR)

$(ORIG_SRC_TAR): $(BUILDDIR)
	tar czf $(ORIG_SRC_TAR) --exclude="$(BUILDDIR)/debian" $(BUILDDIR)

.PHONY: deb
deb: $(DEB)
$(DEB): $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -b -us -uc
	lintian $(DEB)

.PHONY: dsc
dsc: $(DSC)
	$(MAKE) clean
	$(MAKE) $(DSC)
	lintian $(DSC)

$(DSC): $(BUILDDIR) $(ORIG_SRC_TAR)
	cd $(BUILDDIR); dpkg-buildpackage -S -us -uc -d

sbuild: $(DSC)
	sbuild $(DSC)

# HACK, but this is a *very* seldom bumped package and normally 1:1 the same in all our dists
-include /etc/os-release

.PHONY: upload
upload: UPLOAD_DIST ?= $(VERSION_CODENAME)
upload: $(DEB)
	tar cf - $(DEB) | ssh -X repoman@repo.proxmox.com -- upload --product pve,pmg,pbs --dist $(UPLOAD_DIST)

distclean: clean
clean:
	$(MAKE) -C src clean
	rm -rf $(PACKAGE)-[0-9]*/ $(PACKAGE)*tar* *.dsc *.deb *.changes *.build *.buildinfo

.PHONY: dinstall
dinstall: $(DEB)
	dpkg -i $(DEB)
